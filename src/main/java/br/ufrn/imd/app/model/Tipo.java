package br.ufrn.imd.app.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="tipo")
public class Tipo {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_TIPO")
	@SequenceGenerator(name="SEQ_TIPO", sequenceName="seq_tipo", allocationSize=1)
	private int id;
	private String descricao;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
