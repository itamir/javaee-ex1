package br.ufrn.imd.app.bussiness;

import java.util.List;

import javax.ejb.Local;

import br.ufrn.imd.app.model.Tipo;

@Local
public interface TipoService {

	public Tipo salvarOuAtualizar(Tipo tipo);
	
	public List<Tipo> listarTipos();
}
