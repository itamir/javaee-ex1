package br.ufrn.imd.app.dao;

import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.ufrn.imd.app.model.Tipo;

public class TipoDao  {

	@PersistenceContext
    private EntityManager em;
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Tipo salvarOuAtualizar(Tipo tipo) {
		if(tipo.getId() == 0)
			em.persist(tipo);
		else
			em.merge(tipo);
		return tipo;
	}
	
	@SuppressWarnings("unchecked")
	public List<Tipo> listarTipos() {
		return (List<Tipo>) em.createQuery("select t from Tipo t");
	}	
	
}
