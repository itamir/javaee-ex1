package br.ufrn.imd.app.controller;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import br.ufrn.imd.app.bussiness.TipoService;
import br.ufrn.imd.app.model.Tipo;

@ManagedBean
@RequestScoped
public class TipoMBean {

	@EJB
	private TipoService tipoService;
	
	private Tipo tipo;
	
	public TipoMBean() {
		tipo = new Tipo();
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	public String salvar() {
		tipoService.salvarOuAtualizar(tipo);
		tipo = new Tipo();
		return null;
	}
}
